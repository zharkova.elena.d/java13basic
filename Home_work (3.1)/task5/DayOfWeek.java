package Hwork_3_Ch1.task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class DayOfWeek {
    private byte namberWeek; //Номер недели
    private String nameWeek; //Название дня недели
    private static byte chetNamber = 1;

    //---------------------------- Конструктор ----------------------------
    public DayOfWeek() { //Создаем конструктор с начальными параметрами
        namberWeek = chetNamber;
        switch (chetNamber) {
            case 1 -> {
                nameWeek = String.valueOf(WeekDays.MONDAY); //Преобразуем в строку
            }
            case 2 -> {
                nameWeek = String.valueOf(WeekDays.TUESDAY); //Преобразуем в строку
            }
            case 3 -> {
                nameWeek = String.valueOf(WeekDays.WEDNESDAY); //Преобразуем в строку
            }
            case 4 -> {
                nameWeek = String.valueOf(WeekDays.THURSDAY); //Преобразуем в строку
            }
            case 5 -> {
                nameWeek = String.valueOf(WeekDays.FRIDAY); //Преобразуем в строку
            }
            case 6 -> {
                nameWeek = String.valueOf(WeekDays.SATURDAY); //Преобразуем в строку
            }
            case 7 -> {
                nameWeek = String.valueOf(WeekDays.SUNDAY); //Преобразуем в строку
            }
        }
        chetNamber++;
    }

    /** Метод выводящий номер и название дня недели
     *
     */
    public void getName_Num() {
        System.out.print(namberWeek + " ");
        System.out.print(nameWeek + "\n");
    }


}
