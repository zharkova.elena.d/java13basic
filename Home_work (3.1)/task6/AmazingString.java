package Hwork_3_Ch1.task6;

import java.util.regex.Pattern;

/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class AmazingString {
    private char arrChar[];

    //---------------------------- Конструкторы ----------------------------
    public AmazingString(char[] arrChar) { //Принимает символы
        this.arrChar = arrChar;
    }
    public AmazingString(String arrString) { //Принимает строку
        this.arrChar = arrString.toCharArray();
    }

    //---------------------------- Методы ----------------------------

    /** Вернуть i-ый символ строки
     *
     * @param index индекс символа, который нужно вернуть
     * @return
     */
    public char getSymbol(int index) {
        char symbol = 0;
        for (int i = 0; i < this.arrChar.length; i++) {
            if (i == index) {
                symbol = this.arrChar[i];
                break;
            }
        }
        return symbol;
    }

    /** Вернуть длину строки
     *
     * @return
     */
    public int getLength() {
        return this.arrChar.length;
    }

    /** Вывести строку на экран
     *
     */
    public void printStr() {
        for (char e : this.arrChar) {
            System.out.print(e);
        }
        System.out.print("\n");
    }

    /** Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char)
     *
     * @param arrChar
     * @return
     */
    public boolean getPodstr(char[] arrChar) {
        boolean podstroca = false;

        for (int i = 0; i < this.arrChar.length; i++) {
            int index = 0;
            int chet = 1; //Счетчик

            //Если первый символ переданного массива совпадает с символом имеющегося массива
            if (arrChar[index] == this.arrChar[i]) {

                //Проверить сколько символов после найденного элемента осталось в this.arrChar, если меньше чем в arrChar, то false
                if (arrChar.length > (this.arrChar.length - i)) {
                    return false;
                }
                //Если в переданной строке один символ
                if (arrChar.length == 1) {
                    return true;
                }

                //Сравниваем след символ со след символом
                for (int j = i + 1; j < this.arrChar.length; j++) {

                    if ((arrChar[index + 1] != this.arrChar[j])) { //Если не равны
                        break;
                    }

                    if (arrChar[index + 1] == this.arrChar[j]) { //Если равны
                        chet++;
                        index++;
                    }
                    //Если перебрали все символы в arrChar
                    if (chet == arrChar.length) {
                        return true;
                    }
                }

            }

        }
        return podstroca;
    }

    /** Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char)
     *
     * @param arrString
     * @return
     */
    public boolean getPodstr(String arrString) {
        char[] arrChar = arrString.toCharArray(); //Переводим в массив символов

        boolean podstroca = false;

        for (int i = 0; i < this.arrChar.length; i++) {
            int index = 0;
            int chet = 1; //Счетчик

            //Если первый символ переданного массива совпадает с символом имеющегося массива
            if (arrChar[index] == this.arrChar[i]) {

                //Проверить сколько символов после найденного элемента осталось в this.arrChar, если меньше чем в arrChar, то false
                if (arrChar.length > (this.arrChar.length - i)) {
                    return false;
                }
                //Если в переданной строке один символ
                if (arrChar.length == 1) {
                    return true;
                }

                //Сравниваем след символ со след символом
                for (int j = i + 1; j < this.arrChar.length; j++) {

                    if ((arrChar[index + 1] != this.arrChar[j])) { //Если не равны
                        break;
                    }

                    if (arrChar[index + 1] == this.arrChar[j]) { //Если равны
                        chet++;
                        index++;
                    }
                    //Если перебрали все символы в arrChar
                    if (chet == arrChar.length) {
                        return true;
                    }
                }

            }

        }
        return podstroca;
    }

    /** Удалить из строки AmazingString ведущие пробельные символы, если они есть
     *
     * @return
     */
    public char[] DeleteSpace() {
        int positionStart = 0, positionEnd = 0;

        //Запоминаем первую позицию
        for (int i = 0; i < this.arrChar.length; i++) {
            //Если пробелов нет
            if (this.arrChar[i] != ' ') {
                break;
            }
            positionStart = i;
        }
        //Запоминаем вторую позицию
        for (int i = this.arrChar.length - 1; i >= 0; i--) {
            //Если пробелов нет
            if (this.arrChar[i] != ' ') {
                break;
            }
            positionEnd = i;
        }

        char[] arrCharNew = new char[(positionEnd - positionStart) - 1]; //Новый массив

        System.arraycopy(this.arrChar, positionStart + 1, arrCharNew, 0, (positionEnd - positionStart) - 1);
        this.arrChar = arrCharNew;

        return this.arrChar;
    }

    /** Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
     *
     * @return
     */
    public char[] ReverseString() {
        char[] arrCharNew = new char[this.arrChar.length];
        int index = 0;

        for (int i = this.arrChar.length - 1; i >= 0; i--) {
            arrCharNew[index] = this.arrChar[i];
            index++;
        }
        this.arrChar = arrCharNew;
        return this.arrChar;
    }

}
