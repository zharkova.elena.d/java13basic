package Hwork_3_Ch1.task6;

public class Main {
    public static void main(String[] args){
        AmazingString st1 = new AmazingString(new char[] {' ',' ','s', 'o', 's',' '});
        AmazingString st2 = new AmazingString("Math");

        System.out.println(st1.getSymbol(1));
        System.out.println(st2.getSymbol(2));

        System.out.println(st1.getLength());
        System.out.println(st2.getLength());

        st1.printStr();
        st2.printStr();

        System.out.println(st1.getPodstr(new char[] {'o'}));
        System.out.println(st1.getPodstr(new char[] {'s','x'}));

        System.out.println(st2.getPodstr("Math"));
        System.out.println(st2.getPodstr("r"));

        st1.DeleteSpace();
        st2.ReverseString();


    }
}
