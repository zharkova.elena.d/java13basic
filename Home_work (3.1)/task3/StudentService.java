package Hwork_3_Ch1.task3;
/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */
import Hwork_3_Ch1.task2.Student;

public class StudentService {

    /** Метод ищет лучшего студента
     *
     * @param arrStudent массив студентов
     */
    public static void bestStudent(Student[] arrStudent) { //Передается массив студентов

        double maxSred = 0; //Максимальный средний балл
        int studId = -1;

        for (int i = 0; i < arrStudent.length; i++) {
            if (arrStudent[i].sredGrades() > maxSred) { //Вызываем у i-го объекта метод сред. ариф. и если больше максимума
                maxSred = arrStudent[i].sredGrades(); //Присваиваем новое значение
                studId = i; //Запоминаем студента
            }
        }
        System.out.println(arrStudent[studId].getName() + " " + arrStudent[studId].getSurname()); //Выводим имя и фамилию студента
    }

    /** Сортировка студентов по фамилии
     *
     * @param arrStudent массив студентов
     */
    public static void sortBySurname(Student[] arrStudent) {
        Student moveStud;

        for (int i = 0; i < arrStudent.length ; i++) {
            char chara = arrStudent[i].getSurname().charAt(0); //Получаем первый символ

            for (int j = i + 1; j < arrStudent.length; j++) {
                if (chara > arrStudent[j].getSurname().charAt(0)) { //Если символ стоит после
                    moveStud = arrStudent[i];
                    arrStudent[i] = arrStudent[j];
                    arrStudent[j] = moveStud;
                }
            }
        }
    }

}
