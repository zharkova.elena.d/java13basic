package Hwork_3_Ch1.task3;

import Hwork_3_Ch1.task2.Student;

public class Main {
    public static void main (String[] args) {
        Student person1 = new Student();
        Student person2 = new Student();
        Student person3 = new Student();

        person1.setName("Андрей");
        person1.setSurname("Янин");
        person1.setGrades(new int[] {5, 4, 5, 3, 2, 4, 4, 4, 3, 5});

        person2.setName("Илья");
        person2.setSurname("Иванов");
        person2.setGrades(new int[] {4, 3, 4, 3, 4, 3, 4, 3, 3, 5});

        person3.setName("Анастасия");
        person3.setSurname("Семенова");
        person3.setGrades(new int[] {2, 2, 3, 5, 5, 5, 5, 5, 5, 4});

        Student[] sdudents = {person1, person2, person3}; //Создаем массив студентов

        StudentService.bestStudent(sdudents);
        StudentService.sortBySurname(sdudents);
    }
}
