package Hwork_3_Ch1.task8;

public class Atm {
    private static int scetchic;
    private double course;

    //---------------------------- Конструкторы ----------------------------
    /**
     *
     * @param course курс валюты
     */
    public Atm(double course) {
        errorData(course);
        this.course = course;
        scetchic++;

    }
    //---------------------------- Геттеры ----------------------------
    /** Возвращает количество инстансов АТМ
     *
     * @return
     */
    public static int getScetchic() {
        return scetchic;
    }
    //---------------------------- Методы ----------------------------

    /** Перевод переданной суммы из долларов в рубли по курсу
     *
     * @param money переданная сумма долларов
     * @return
     */
    public double transferRub(double money) {
        errorData(money);
        double sum = money * this.course;
        return sum;
    }

    /** Перевод переданной суммы из рублей в доллары по курсу
     *
     * @param money переданная сумма рублей
     * @return
     */
    public double transferDollar(double money) {
        errorData(money);
        double sum = money / this.course;
        return sum;
    }

    /** Проверка на положительность введенных занчений
     *
     * @param data проверяемое значение
     */
    private void errorData(double data) {
        if (data < 0) {
            System.out.println("Введены отрицательные значения!");
            System.exit(1);
        }
    }

}
