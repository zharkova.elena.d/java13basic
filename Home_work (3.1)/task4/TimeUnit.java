package Hwork_3_Ch1.task4;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
 */
public class TimeUnit {
    private int hour; //Часы
    private int min; //Минуты
    private int sec; //Секунды

    //---------------------------- Конструкторы ----------------------------
    public TimeUnit(int hour, int min, int sec) {
        this.hour = hour;
        this.min = min;
        this.sec = sec;
        errorData();

    }
    public TimeUnit(int hour, int min) {
        this.hour = hour;
        this.min = min;
        this.sec = 0;
        errorData();
    }
    public TimeUnit(int hour) {
        this.hour = hour;
        this.min = 0;
        this.sec = 0;
        errorData();
    }

    //---------------------------- Геттеры ----------------------------
    public int getHour() {
        return hour;
    }
    public int getMin() {
        return min;
    }
    public int getSec() {
        return sec;
    }

    //---------------------------- Сеттеры ----------------------------
    public void setHour(int hour) {
        this.hour = hour;
    }
    public void setMin(int min) {
        this.min = min;
    }
    public void setSec(int sec) {
        this.sec = sec;
    }

    //---------------------------- Методы ----------------------------

    /** Метод выводит введенное время в 24-х часовом формате
     *
     */
    public void currentTime_24() {
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR_OF_DAY, this.hour);
        time.set(Calendar.MINUTE, this.min);
        time.set(Calendar.SECOND, this.sec);
        DateFormat date = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Установленое время: " + date.format(time.getTime()));
    }

    /** Метод выводит введенное время в 12-ти часовом формате
     *
     */
    public void currentTime_12() {
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR_OF_DAY, this.hour);
        time.set(Calendar.MINUTE, this.min);
        time.set(Calendar.SECOND, this.sec);
        DateFormat date = new SimpleDateFormat("hh:mm:ss aa");
        System.out.println("Установленое время: " + date.format(time.getTime()));
    }

    /** Метод, который прибавляет переданное время к установленному в TimeUnit
     *
     * @param hour чсаы
     * @param min минуты
     * @param sec секунду
     */
    public void plusTime(int hour, int min, int sec) {
        this.hour += hour;
        this.min += min;
        this.sec += sec;
    }

    /** Метод для распознавания ошибок при вводе
     *
     */
    private void errorData() {
        if ((this.hour < 0)||(this.hour >= 60)) {
            System.out.println("Введены некорректные часы!");
            System.exit(1);
        }
        if ((this.min < 0)||(this.min >= 60)) {
            System.out.println("Введены некорректные минуты!");
            System.exit(1);
        }
        if ((this.sec < 0)||(this.sec >= 60)) {
            System.out.println("Введены некорректные секунды!");
            System.exit(1);
        }
    }
}
