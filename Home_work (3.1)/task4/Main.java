package Hwork_3_Ch1.task4;

public class Main {
    public static void main (String[] args) {
        TimeUnit time = new TimeUnit(2, 3,55);
        time.currentTime_24();

        TimeUnit time1 = new TimeUnit(12, 17,0);
        time1.currentTime_24();

        TimeUnit time2 = new TimeUnit(23,0,0);
        time2.currentTime_24();

        TimeUnit time3 = new TimeUnit(11,9,2);
        time3.currentTime_12();

        TimeUnit time4 = new TimeUnit(22,9,2);
        time4.currentTime_12();

        TimeUnit time5 = new TimeUnit(10,20,50);
        time5.plusTime(7, 10, 7);
        time5.currentTime_24();

    }
}
