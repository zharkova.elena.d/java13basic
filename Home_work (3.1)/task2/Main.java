package Hwork_3_Ch1.task2;

public class Main {
    public static void main (String[] args) {
        Student st = new Student();

        st.setName("Надя");
        st.setSurname("Моисеинко");

        st.newGrades(5);
        st.newGrades(3);
        st.newGrades(3);
        st.newGrades(4);

        System.out.println(st.sredGrades());

        for (int e : st.getGrades()) {
            System.out.print(e + " ");
        }
    }
}
