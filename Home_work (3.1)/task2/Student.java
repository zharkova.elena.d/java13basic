package Hwork_3_Ch1.task2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
public class Student {
    private String name; //Имя студента
    private String surname; //Фамилия студента
    private int[] grades = new int[10]; //Последние 10 оценок студента

    //---------------------------- Геттеры ----------------------------
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
    public int[] getGrades() {
        return grades;
    }

    //---------------------------- Сеттеры ----------------------------
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    //---------------------------- Методы ----------------------------

    //Метод добавляющий новую оценку
    public void newGrades(int grades) {
        for (int i = 1; i < getGrades().length; i++) {
            this.grades[i - 1] = this.grades[i];
        }
        this.grades[getGrades().length - 1] = grades;
    }

    //Средний балл студента
    public double sredGrades() {
        int sum = 0;
        int n = 0;
        for (int i = 0; i < getGrades().length; i++) {
            sum += this.grades[i];
        }
        for (int i = 0; i < getGrades().length; i++) { //Проверяем сколько не пустых ячеек
            if (this.grades[i] != 0) {
                n++;
            }
        }
        return (double) sum / (double)n;
    }

}
