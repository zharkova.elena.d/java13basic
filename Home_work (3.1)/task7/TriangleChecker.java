package Hwork_3_Ch1.task7;
/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */
public class TriangleChecker {

    /** Возможно составить из них треугольник
     *
     * @param a первая сторона треугольника
     * @param b вторая сторона треугольника
     * @param c третья сторона треугольника
     * @return
     */
    public static boolean chekTriangle(double a, double b, double c) {
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            return true;
        }
        else {
            return false;
        }
    }
}
