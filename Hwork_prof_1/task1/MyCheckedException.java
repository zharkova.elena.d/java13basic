package HOMEWORKS.PROF.Hwork_prof_1.task1;

public class MyCheckedException extends Exception{

    public MyCheckedException() {

    }

    public MyCheckedException(String message) { //Конструктор с сообщением
        super(message);
    }
}
