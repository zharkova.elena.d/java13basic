package HOMEWORKS.PROF.Hwork_prof_1.task1;

/*
Создать собственное исключение MyCheckedException, являющееся
проверяемым.
 */
public class Main {
    public static void main(String[] args) {

        //checkedexeption мы обязаны ЯВНО обрабатывать!!! С помощью try-catch
        try
        { //Блок кода где может произойти исключение
            exampleMethod(1,2);
            exampleMethod(-1, 0);
        }
        catch (MyCheckedException e) { //Обработка исключения
            System.out.println(e.getMessage());
        }
        finally {
        }
    }
    public static void exampleMethod(int a, int b) throws MyCheckedException { //Предупреждение что метод может выбросить исключение
        if (a + b < 0) {
            throw new MyCheckedException("sum is negative!"); //Возбуждение исключения
        }
        else {
            System.out.println(a + b);
        }

}

}
