package HOMEWORKS.PROF.Hwork_prof_1.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try {
            int n = inputN();
        } catch (Exception e) {
            throw new RuntimeException(e); //Ловим исключение
        }

    }
    private static int inputN() throws Exception { //Показываем что в этом методе мы можем словить исключение
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 100 || n < 0) {
            throw new Exception("Неверные данные!"); //Вызываем исключение
        }

        return n;
    }

}
