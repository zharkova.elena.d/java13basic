package HOMEWORKS.PROF.Hwork_prof_1.task4;

/*
Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

public class MyEvenNumber {
    private int number;

    public MyEvenNumber(int number) throws OddNumbersExeption {
        if (number % 2 == 0) {
            this.number = number;
        }
        else {
            throw new OddNumbersExeption("Number is no odd!");
        }
    }
}
