package HOMEWORKS.PROF.Hwork_prof_1.task4;

public class OddNumbersExeption extends RuntimeException { //Ошибка при вводе нечетных чисел

    public OddNumbersExeption(String message) { //Конструктор с сообщением
        super(message);
    }
}
