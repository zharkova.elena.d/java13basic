package HOMEWORKS.PROF.Hwork_prof_1.task2;

/*
Создать собственное исключение MyUncheckedException, являющееся
непроверяемым.
 */
public class Main {
    public static void main(String[] args) {

        //в unchecfedexeption НЕ НУЖНО ЯВНО обрабатывать!!! Можно сделать это в вызванном методе
        exampleMethod(1,2);
        exampleMethod(-1, 0);

    }
    public static void exampleMethod(int a, int b) throws MyUncheckedException { //Предупреждение что метод может выбросить исключение
        if (a + b < 0) {
            throw new MyUncheckedException("sum is negative!"); //Возбуждение исключения
        }
        else {
            System.out.println(a + b);
        }

}

}
