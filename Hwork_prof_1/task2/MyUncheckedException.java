package HOMEWORKS.PROF.Hwork_prof_1.task2;

public class MyUncheckedException extends ArithmeticException{

    public MyUncheckedException() {

    }

    public MyUncheckedException(String message) { //Конструктор с сообщением
        super(message);
    }
}
