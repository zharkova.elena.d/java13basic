package HOMEWORKS.PROF.Hwork_prof_1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

/*
Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
./output.txt текст из input, где каждый латинский строчный символ заменен на
соответствующий заглавный. Обязательно использование try с ресурсами.
 */

public class readAndWriteFile {
        private static final String PKG_DIRECTORY = "/C:\\Users\\Елена\\IdeaProjects\\untitled\\src\\HOMEWORKS\\PROF\\Hwork_prof_1" +
                "\\task3\\Files";
        private static final String OUTPUT_FILE_NAME = "output.txt";
        private static final String INPUT_FILE_NAME = "input.txt";

        public static void main(String[] args) {
            try {
                readAndWriteFile();
            }
            catch (IOException e) {
                System.out.println("FileReadWrite#main!error: " + e.getMessage());
            }
        }

        public static void readAndWriteFile() throws IOException {
            Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME)); //Чтение
            Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME); //Написание

            try (scanner; writer) {
                while (scanner.hasNext()) {
                    writer.write(scanner.nextLine().toUpperCase() + "\n");
                }
            }
        }
}
