package HOMEWORKS.PROF.Hwork_prof_1.task6;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {

    public String name;
    public String birthday;
    public String gender;
    public double height;

    //Имя
    public void checkName(@NotNull String str) {
        if (str.matches("^([A-Z][a-z]{2,20})")) {
            this.name = str;
        }
        else {
            throw new CorrectDataExeption.NameDataExeption("Incorrect name!");
        }
    }

    //Дата рождения
    public void checkBirthdate(@NotNull String str) throws ParseException {

        //Текущая дата и время
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy"); //Формат даты
        Date date_after = new Date(System.currentTimeMillis()); //Получаем текущую дату
        Date date_before = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.1900"); //Получаем минимальную дату

        if (str.matches("(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).((19|20)\\d\\d)")) { //Если дата корректная

            Date date_str = new SimpleDateFormat("dd.MM.yyyy").parse(str); //Преобразуем полученную строку в объект Data

            int ret_before = date_str.compareTo(date_before); //Смотрим стоит ли наша дата до аргумента
            int ret_after = date_str.compareTo(date_after); //Смотрим стоит ли наша дата после аргумента

            if ((ret_before >= 0) && (ret_after <= 0)) {
                this.birthday = str;
            }

        }
        else {
            throw new CorrectDataExeption.BirthdateExeption("Incorrect data birthday!");
        }
    }

    //Пол
    public void checkGender(@NotNull String str) {
        if (str.matches("(^(F|f)emale$)")) { //Если женщина
            this.gender = String.valueOf(Gender.FEMALE);
        }
        else if (str.matches("(^(M|m)ale$)")) { //Если мужчина
            this.gender = String.valueOf(Gender.MALE);
        }
        else {
            throw new CorrectDataExeption.GenderDataExeption("Incorrect gender!");
        }

    }

    //Рост
    public void checkHeight(@NotNull String str) {
        if (str.matches("^[0-9]*[.,]?[0-9]+$")) { //Если строчка корректная
            double number = Double.parseDouble(str); //Преобразуем в числовой тип

            if (number > 0) { //Больше нуля
                this.height = number;
            }
        }
        else {
            throw new CorrectDataExeption.HeightDataExeption("Incorrect height!");
        }

    }
}
