package HOMEWORKS.PROF.Hwork_prof_1.task6;

public class CorrectDataExeption extends RuntimeException {
    public CorrectDataExeption(String message) { //Конструктор с сообщением
        super(message);
    }

    public static class NameDataExeption extends CorrectDataExeption{
        public NameDataExeption(String message) {
            super(message);
        }
    }

    public static class BirthdateExeption extends CorrectDataExeption{
        public BirthdateExeption(String message) {
            super(message);
        }
    }

    public static class GenderDataExeption extends CorrectDataExeption{
        public GenderDataExeption(String message) {
            super(message);
        }
    }

    public static class HeightDataExeption extends CorrectDataExeption{
        public HeightDataExeption(String message) {
            super(message);
        }
    }
}
