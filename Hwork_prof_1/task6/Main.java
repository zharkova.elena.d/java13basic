package HOMEWORKS.PROF.Hwork_prof_1.task6;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) {

        FormValidator User1 = new FormValidator();
        User1.checkName("Anna");
        User1.checkGender("Female");
        User1.checkHeight("165");

        try {
            User1.checkBirthdate("21.01.2001");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }
}
