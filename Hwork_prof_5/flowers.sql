/*
Таблица цветы
- id ключ
- title наименование
- cost стоимость
*/
create table flowers
(
    id_flowers serial primary key, --уникальное значение ключа которое самостоятельно появляется
    title varchar(30) not null,
    cost int
);

select *
from flowers

insert into flowers(title, cost)
values ('Розы', 100)

insert into flowers(title, cost)
values ('Лилии', 50)

insert into flowers(title, cost)
values ('Ромашки', 25)



