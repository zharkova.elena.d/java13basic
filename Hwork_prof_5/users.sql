/*
Таблица пользователи
- id ключ
- name_user имя
- number_user номер телефона
*/
create table users
(
    id_users serial primary key, --уникальное значение ключа которое самостоятельно появляется
    name_user varchar(30) not null,
    number_user varchar(30) not null
);

select *
from users

insert into users(name_user, number_user)
values ('Хабибулина А.А.', '+79244052119')

insert into users(name_user, number_user)
values ('Доморацкая А.О.', '+79292014118')

insert into users(name_user, number_user)
values ('Семенов П.С.', '+79215091217')