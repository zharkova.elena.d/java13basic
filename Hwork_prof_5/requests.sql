
--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select *
from orders o,
     users u
where o.id = 3 and o.user_id = u.id_users;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select *
from orders
where order_date >= date_trunc('month',current_timestamp - interval '1 month')
   and order_date <  date_trunc('month',current_timestamp)

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select o.id, o.quantity, f.title
from orders o
join flowers f on o.flower_id = f.id_flowers
where o.quantity = (select max(quantity) from orders)

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(cost_order) as "Общая выручка"
from orders