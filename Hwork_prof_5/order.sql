/*
Таблица заказ
- id ключ
- id_user заказчик
- id_flower цветы
- quantity количество
- cost_order стоимость заказа
- order_date дата заказа
- delivery_date дата доставки
*/
create table orders
(
    id serial primary key, --уникальное значение ключа которое самостоятельно появляется
    user_id integer REFERENCES users (id_users),
    flower_id integer REFERENCES flowers (id_flowers),
    quantity integer check (quantity > 0 and quantity <= 1000),
    cost_order integer check (cost_order > 0),
    order_date timestamp not null,
    delivery_date date not null
);

select *
from orders

insert into orders(user_id, flower_id, quantity, cost_order, order_date, delivery_date)
values (1, 2, 55,
(select cost
from flowers
where id_flowers = 2) * 55,
now(), '2023-05-08')

insert into orders(user_id, flower_id, quantity, cost_order, order_date, delivery_date)
values (2, 1, 44,
(select cost
from flowers
where id_flowers = 1) * 44,
now(), '2023-06-21')


insert into orders(user_id, flower_id, quantity, cost_order, order_date, delivery_date)
values (2, 3, 13,
(select cost
from flowers
where id_flowers = 2) * 13,
now(), '2023-05-09')

insert into orders(user_id, flower_id, quantity, cost_order, order_date, delivery_date)
values (1, 2, 89,
(select cost
from flowers
where id_flowers = 2) * 89,
now() - interval '1 month', '2023-02-03')
