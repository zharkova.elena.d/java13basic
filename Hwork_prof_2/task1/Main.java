package HOMEWORKS.PROF.Hwork_prof_2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
 */

public class Main {
    public static void main(String[] args) {

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("молоко");
        stringList.add("сахар");
        stringList.add("кукуруза");
        stringList.add("сахар");
        stringList.add("конфеты");
        stringList.add("молоко");

        returnUniqueCollection(stringList);

        ArrayList<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(4);

        returnUniqueCollection(integerList);

    }
    public static <T> void returnUniqueCollection(ArrayList<T> list) {
        Set<T> setList = new HashSet<>(list);

        for ( T elements : setList) {
            System.out.println(elements);
        }
    }




}
