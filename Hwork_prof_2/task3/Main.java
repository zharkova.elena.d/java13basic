package HOMEWORKS.PROF.Hwork_prof_2.task3;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/*
Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        PowerfulSet.intersection(set1, set2);
        //PowerfulSet.union(set1, set2);
        //PowerfulSet.relativeComplement(set1, set2);
    }

    public static class PowerfulSet {

        private PowerfulSet() {
        }

        public static <T> void intersection(Set<T> set1, Set<T> set2) { //Пересечение двух наборов
            set1.retainAll(set2);
            System.out.println(set1);

        }

        public static <T> void union(Set<T> set1, Set<T> set2) { //Объединение двух наборов
            set1.addAll(set2);
            System.out.println(set1);
        }

        public static <T> void relativeComplement(Set<T> set1, Set<T> set2) { //Вычитание двух наборов
            set1.removeAll(set2);
            System.out.println(set1);
        }
    }

}
