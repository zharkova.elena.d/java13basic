package HOMEWORKS.PROF.Hwork_prof_2.task2;

/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        String t = scanner.next();

        System.out.println(stringAnagram(s, t));
    }

    public static boolean stringAnagram(String firstString, String secondString) {

        if (firstString.length() != secondString.length()) {
            return false;
        }

        Map<Character, Integer> firstMap = new HashMap<>();
        for (char element : firstString.toLowerCase().toCharArray()) { //Кладем элементы из первой строчки в map
            firstMap.put(element, firstMap.getOrDefault(element, 0) + 1);
        }

        Map<Character, Integer> secondMap = new HashMap<>();
        for (char element : secondString.toLowerCase().toCharArray()) { //Кладем элементы из первой строчки в map
            secondMap.put(element, secondMap.getOrDefault(element, 0) + 1);
        }

        return firstMap.equals(secondMap);
    }
}
