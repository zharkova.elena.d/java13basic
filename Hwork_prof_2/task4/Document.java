package HOMEWORKS.PROF.Hwork_prof_2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
 */

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return " Document {" +
                "id = " + id +
                ", name = '" + name + '\'' +
                ", pageCount = " + pageCount +
                '}';
    }
}
