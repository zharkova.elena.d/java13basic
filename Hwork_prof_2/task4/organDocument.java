package HOMEWORKS.PROF.Hwork_prof_2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class organDocument {

    private organDocument() {
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> mapDocuments = new HashMap<>(); //Создаем map
        for (Document element : documents) {
            mapDocuments.put(element.id, element);
        }
        return mapDocuments;
    }
}
