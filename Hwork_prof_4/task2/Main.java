package HOMEWORKS.PROF.Hwork_prof_4.task2;

import java.util.ArrayList;
import java.util.List;

/*
2. На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
public class Main {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 6);
        int result = list.stream()
                .reduce(1, (x, y) -> x * y ); //Выполняем операцию с шагом в единицу на двух числах
        System.out.println(result);

    }
}
