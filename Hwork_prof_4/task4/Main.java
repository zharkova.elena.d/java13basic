package HOMEWORKS.PROF.Hwork_prof_4.task4;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/*
4. На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(1.0, 2.0, 4.0, 3.0, 6.0);
        List<Double> sortedList = list.stream()
                .sorted(Collections.reverseOrder()) //Сортируем список и переворачиваем его
                .collect(Collectors.toList()); //Возвращаем новый список
        System.out.println(sortedList);
    }
}
