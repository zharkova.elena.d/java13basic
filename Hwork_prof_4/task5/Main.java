package HOMEWORKS.PROF.Hwork_prof_4.task5;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
5. На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");

        System.out.println(list.stream()
                .map(String::toUpperCase) //Приводим к верхнему регистру
                .collect(Collectors.joining(", "))); //Добавляем запятые

    }
}
