package HOMEWORKS.PROF.Hwork_prof_4.task1;

import java.util.stream.IntStream;

/*
1. Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
public class Main {
    public static void main(String[] args) {

        int arr;
        arr = IntStream.rangeClosed(1, 100) //Создаем последовательность чисел
                .filter(x -> x % 2 == 0) //Фильтруем все четные
                .sum(); //Суммируем
        System.out.println(arr);
    }
}
