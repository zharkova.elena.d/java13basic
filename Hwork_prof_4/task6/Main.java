package HOMEWORKS.PROF.Hwork_prof_4.task6;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
6. Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */
public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<>(Set.of(1, 2, 3));
        Set<Integer> set2 = new HashSet<>(Set.of(4, 5, 6));
        Set<Integer> set3 = new HashSet<>(Set.of(7, 8, 9));

        System.out.println(Arrays.toString(Stream.of(set1, set2, set3)
                .flatMap(Collection::stream).
                distinct().
                toArray()));

    }
}
