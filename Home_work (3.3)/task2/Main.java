package Hwork_3_Ch3.task2;

import Hwork_3_Ch3.task2.Furniture.Chair;
import Hwork_3_Ch3.task2.Furniture.Table;

public class Main {
    public static void main(String[] args){

        Chair chair = new Chair();
        Table table = new Table();

        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        System.out.println(bestCarpenterEver.checkFurniture(chair));
        System.out.println(bestCarpenterEver.checkFurniture(table));
    }
}
