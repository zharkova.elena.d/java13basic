package Hwork_3_Ch3.task4;

import java.util.ArrayList;
import java.util.Arrays;

import static Hwork_3_Ch3.task4.Participant.getCout_Participant;

public class Rating {

    private static ArrayList<Participant> participantArrayList = new ArrayList<>(); //Список участников
    private static ArrayList<Dog> dogArrayList = new ArrayList<>(); //Список собак
    private static int[][] ratingArray = new int[getCout_Participant()][3]; //Массив с оценками


    //---------------------------- Конструкторы ----------------------------
    private Rating(){
    }

    //---------------------------- Методы ----------------------------
    /** Добавляем участников в список
     *
     * @param participant набор участников
     */
    public static void to_ParticipantList(Participant ... participant) { //Добавляем участников в список
        participantArrayList.addAll(Arrays.asList(participant)); //Добавляем все в список
    }

    /** Добавляем собак в список
     *
     * @param dog
     */
    public static void to_DogList(Dog ... dog) { //Добавляем участников в список
        dogArrayList.addAll(Arrays.asList(dog)); //Добавляем все в список
    }

    /** Добавляем оценки
     *
     * @param rating набор оценок
     */
    public static void set_Rating(int ... rating) {

        int cout = 0; //Счетчик для перемещения по rating[]

        for (int i = 0; i < getCout_Participant(); i++) {

            for (int j = 0; j < 3; j++) {

                if (cout + 1 != rating.length) {
                    ratingArray[i][j] = rating[cout];
                    cout++; //Увеличиваем счетчик после каждой итерации
                } else {
                    ratingArray[i][j] = rating[cout];
                }
            }
        }
    }

    public static void sred_Rating() {

        double m1 = 0, m2 = 0, m3 = 0; //Призовые места
        int i1 = 0, i2 = 0, i3 = 0;

        //Высчитываем среднее арифметичесоке
        for (int i = 0; i < ratingArray.length; i++) {
            double sred = 0;
            for (int j = 0; j < 3; j++) {
                sred += ratingArray[i][j];
            }
            sred /= 3;

            if (i == 0) { //Прошла первая итерация
                m1 = sred;
                i1 = i;
            } else {
                if (sred > m1) { //Если есть больше то сдвигаем
                    m3 = m2; //3е место
                    i3 = i2;

                    m2 = m1; //2е место
                    i2 = i1;

                    m1 = sred; //1е место
                    i1 = i;
                }
            }
        }

        System.out.println((participantArrayList.get(i1).getName_Participant() + ": " + dogArrayList.get(i1).getName_Dog() + ", " + (int)(m1 * 10)/10.0));
        System.out.println((participantArrayList.get(i2).getName_Participant() + ": " + dogArrayList.get(i2).getName_Dog() + ", " + (int)(m2 * 10)/10.0));
        System.out.println((participantArrayList.get(i3).getName_Participant() + ": " + dogArrayList.get(i3).getName_Dog() + ", " + (int)(m3 * 10)/10.0));



      /*  System.out.println(participantArrayList.get(i1) + ": " + pet[i1] + ", " + (int)(m1 * 10)/10.0 + "\n" +
                name[i2] + ": " + pet[i2] + ", " + (int)(m2 * 10)/10.0 + "\n" +
                name[i3] + ": " + pet[i3] + ", " + (int)(m3 * 10)/10.0 + "\n");

       */

    }







}
