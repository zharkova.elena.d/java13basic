package Hwork_3_Ch3.task4;

import java.util.ArrayList;

public class Participant {

    private static int cout_Participant = 0; //Количество участников
    private String name_Participant;

    private static int index_Participant = 0; //Индекс участника связанный с собакой

    //---------------------------- Конструкторы ----------------------------
    /**
     *
     * @param name_Participant - имя участника
     */
    public Participant(String name_Participant) {
        this.name_Participant = name_Participant;
        cout_Participant++;
    }

    //---------------------------- Геттеры ----------------------------
    public String getName_Participant() {
        return name_Participant;
    }

    public static int getCout_Participant() {
        return cout_Participant;
    }


}
