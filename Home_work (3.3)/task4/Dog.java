package Hwork_3_Ch3.task4;

import java.util.ArrayList;

public class Dog {

    private String name_Dog;

    //---------------------------- Конструкторы ----------------------------
    /**
     *
     * @param name_Dog - имя собаки участника
     */
    public Dog(String name_Dog) {
        this.name_Dog = name_Dog;
    }

    //---------------------------- Геттеры ----------------------------

    public String getName_Dog() {
        return name_Dog;
    }
}
