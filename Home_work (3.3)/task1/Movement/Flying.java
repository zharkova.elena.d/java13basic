package Hwork_3_Ch3.task1.Movement;

public interface Flying extends Movement {

    @Override
    public default void Move() {
        System.out.println("Я летаю!");
    }

}
