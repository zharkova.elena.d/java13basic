package Hwork_3_Ch3.task1.Animals;

import Hwork_3_Ch3.task1.Animal_classification.Bird;
import Hwork_3_Ch3.task1.Movement.Flying;

public class Eagle extends Bird implements Flying {

    @Override
    public void Move() {
        Flying.super.Move(); //Вызывем метод из интерфейса
        System.out.print("И очень быстро!");
    }
}
