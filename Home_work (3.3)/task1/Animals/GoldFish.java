package Hwork_3_Ch3.task1.Animals;

import Hwork_3_Ch3.task1.Animal_classification.Fish;
import Hwork_3_Ch3.task1.Movement.Swimming;

public class GoldFish extends Fish implements Swimming {
    @Override
    public void Move() {
        Swimming.super.Move(); //Вызывем метод из интерфейса
        System.out.print("И очень быстро!");
    }
}
