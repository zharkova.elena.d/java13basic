package Hwork_3_Ch3.task1.Animals;

import Hwork_3_Ch3.task1.Animal_classification.Mammals;
import Hwork_3_Ch3.task1.Movement.Flying;

public class Bat extends Mammals implements Flying {

    @Override
    public void Move() {
        Flying.super.Move(); //Вызывем метод из интерфейса
        System.out.print("И довольно медленно!");
    }



}
