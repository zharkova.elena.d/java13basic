package Hwork_3_Ch3.task3;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {

        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt(); //Строки
        int n = scanner.nextInt(); //Столбцы

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
        ArrayList<Integer> list;

        for (int i = 0; i < m; i++) {
            list = new ArrayList<>(); //Создаем ссылку на новую строку
            for (int j = 0; j < n; j++) {
                list.add(i + j); //Добавляем элемены в каждый столбец
            }
            matrix.add(list); //Добавляем в список matrix, список list (строку)
        }

    }
}
