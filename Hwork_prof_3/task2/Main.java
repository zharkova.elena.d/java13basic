package HOMEWORKS.PROF.Hwork_prof_3.task2;

/*
2. Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
 */

import HOMEWORKS.PROF.Hwork_prof_3.task1.IsLike;

public class Main {
    public static void main(String[] args) {

        check_IsLike(Class_IsLike.class);

    }

    public static void check_IsLike(Class<?> cls) { //Метод проверяющий наличие аннотации isLike, на вход подается какой-то класс
        if (!cls.isAnnotationPresent(IsLike.class)) {
            System.out.println("Warning! No IsLike annotation");
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("IsLike value: " + isLike.value()); //Получаем значение
    }

}

