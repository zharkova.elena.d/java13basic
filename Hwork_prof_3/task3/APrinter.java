package HOMEWORKS.PROF.Hwork_prof_3.task3;

/*
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class APrinter {

    public void print(int a) {
        System.out.println(a);
    }

    public static void invokePrintMethod() {
        try {
            APrinter aPrinter = new APrinter(); //Создаем объект класса принтер
            Class<APrinter> cls = APrinter.class; //Конструируем класс

            Method getPrintMethod = cls.getDeclaredMethod("print", int.class); //Возвращаем метод принт
            System.out.println(getPrintMethod.invoke(aPrinter, 123));
        }

        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Error in call method! " + e.getMessage());
        }
    }

}

