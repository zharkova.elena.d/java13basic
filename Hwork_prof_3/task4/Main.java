package HOMEWORKS.PROF.Hwork_prof_3.task4;

import java.util.ArrayList;
import java.util.List;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

public class Main {

    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());
        }

    }

    public static List<Class<?>> getAllInterfaces1(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        else {
            List<Class<?>> interfaces = new ArrayList<>();
            getAllInterfacesOfParents(cls, interfaces);
            return interfaces;
        }
    }

    public static void getAllInterfacesOfParents(Class<?> cls, List<Class<?>> interfacesFound) {
        while (cls != null) {

            Class<?>[] interfaces = cls.getInterfaces();

            for (Class<?> anInterface : interfaces) {
                if (!interfacesFound.contains(anInterface)) {
                    interfacesFound.add(anInterface);
                    getAllInterfacesOfParents(anInterface, interfacesFound);
                }
            }
            cls = cls.getSuperclass();
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) { //Получаем все интерфейсы класса
        List<Class<?>> interfaces = new ArrayList<>();

        while (cls != Object.class) { //Пока мы не дошли до конца классов-родителей

            for (Class<?> anInterface : cls.getInterfaces()) { //Получили интерфейсы текущего класса

                interfaces.add(anInterface); //Добавляем в лист этот интерфейс
                Class<?>[] arrayInterface = anInterface.getInterfaces(); //Получаем интерфейс текущего элемента

                while (arrayInterface.length > 0) {
                    for (Class<?> elementInterface : arrayInterface) {
                        anInterface = elementInterface;
                        interfaces.add(anInterface);
                        arrayInterface = anInterface.getInterfaces();
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
    }

}
