package Hwork_3_Ch2;

public class Main {
    public static void main(String[] args){

        Library library_one = new Library();

        System.out.println("1. Добавить книгу");
        Book b1, b2, b3, b4, b5, b6;
        library_one.add_NewBook(b1 = new Book("Петля", "Haemuri"));
        library_one.add_NewBook(b2 = new Book("Луна на обрыве", "hyeon minye"));
        library_one.add_NewBook(b3 = new Book("Бездомный Бог", "ADACHI Toka"));
        library_one.add_NewBook(b4 = new Book("Бездомный Бог Омаке", "ADACHI Toka"));
        library_one.add_NewBook(b5 = new Book("Читра", "Taeseon"));

        library_one.add_NewBook(b6 = new Book("Горизонт", "Jeong Ji Hun"));
        library_one.add_NewBook(new Book("Горизонт", "Jeong Ji Hun"));

        System.out.println();
        System.out.println("2. Удалить книгу");
        library_one.delete_Book("Горизонт");

        System.out.println();
        System.out.println("3. Найти книгу по названию");
        library_one.search_BookName("Читра");

        System.out.println();
        System.out.println("4. Найти список книг по автору");
        library_one.search_BookAuthor("ADACHI Toka");

        System.out.println();
        System.out.println("5. Одолжить книгу посетителю по названию, если выполнены все условия");

        Visitor visitor_Semenova_AI;
        Visitor visitor_Savchenko_PS;
        Visitor visitor_Domorackaya_AS;

        library_one.lend_Book(visitor_Semenova_AI = new Visitor("Семенова Анастасия Иванова"), "Петля");
        library_one.lend_Book(visitor_Semenova_AI, "Читра");
        library_one.lend_Book(visitor_Savchenko_PS = new Visitor("Савченко Павел Сергеевич"), "Петля");
        library_one.lend_Book(visitor_Domorackaya_AS = new Visitor("Доморацкая Александра Сергеевна"), "Луна на обрыве");
        library_one.lend_Book(visitor_Domorackaya_AS, "Волчица и пряности");

        System.out.println();
        System.out.println("6. Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу");
        library_one.send_Book(visitor_Semenova_AI, "Петля");
        library_one.send_Book(visitor_Savchenko_PS, "Луна на обрыве");
        library_one.send_Book(visitor_Savchenko_PS, "Бездомный Бог");
        library_one.send_Book(visitor_Savchenko_PS, "Дорохедоро");

        //8.Добавить функционал оценивания книг посетителем при возвращении в библиотеку
        library_one.lend_Book(visitor_Savchenko_PS, "Петля");
        library_one.send_Book(visitor_Savchenko_PS, "Петля");

        library_one.get_GradeBook(b1);

    }
}
