package Hwork_3_Ch2;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

//Класс книги
public class Book {
    private String nameBook = "", nameAuthor = ""; //Название книги и автор
    private String name_Visitor, id_Visitor; //Имя посетителя который взял книгу и id посетителя который взял книгу

    public ArrayList<Double> gradeList = new ArrayList<>(); //Список со всеми оценками книги

    //---------------------------- Конструкторы ----------------------------
    public Book(String nameBook, String nameAuthor) { //Создаем экземпляр книги с указанием названия и автора
        this.nameBook = nameBook;
        this.nameAuthor = nameAuthor;
        this.name_Visitor = null;
        this.id_Visitor = null;
    }

    //---------------------------- Геттеры ----------------------------
    public String getNameBook() {
        return nameBook;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public String getName_Visitor() {
        return name_Visitor;
    }

    public String getId_Visitor() {
        return id_Visitor;
    }

    //---------------------------- Сеттеры ----------------------------
    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public void setName_Visitor(String name_Visitor) {
        this.name_Visitor = name_Visitor;
    }

    public void setId_Visitor(String id_Visitor) {
        this.id_Visitor = id_Visitor;
    }

    //---------------------------- Методы ----------------------------

    //Переопределяем метод equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getNameBook().equals(book.getNameBook());
    }

}
