package Hwork_3_Ch2;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Library {

    private ArrayList<Book> bookList = new ArrayList<>(); //Список книг в библиотеке

    //---------------------------- Методы ----------------------------

    /** 1.Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет в библиотеке.
     *
     * @param book название и автор книги
     */
    public void add_NewBook(Book book) {
        if (!(book.getNameAuthor().isEmpty())) { //Автор не пустой
            if (!bookList.contains(book)) { //Если в списке нет этой книги
                bookList.add(book); //Добавляем книгу
                System.out.println("Книга под названием " + book.getNameBook() + ", автора " + book.getNameAuthor() + ", добавлена в библиотеку!");
            }
            else {
                System.out.println("Книга " + book.getNameBook() + " уже есть в библиотеке!");
            }
        }
        else {
            System.out.println("Не заполнено поле автора!");
        }
    }

    /** 2.Удалить книгу из библиотеки по названию, если такая книга в принципе есть в библиотеке и она в настоящий момент не одолжена
     *
     * @param nameBook название книги
     */
    public void delete_Book(String nameBook) {
        boolean search = false; //Указатель нашли мы книгу или нет

        for (int i = 0; i < bookList.size(); i++) {
            if (bookList.get(i).getNameBook() == nameBook) { //Если элемент из списка совпадает с введенным именем

                if (bookList.get(i).getName_Visitor() == null) { //Если книга не одолжена

                    bookList.remove(i); //Удаляем элемент из списка
                    System.out.println("Книга " + nameBook + " удалена из библиотеки!");
                    search = true;
                }
                else { //Если книга одолжена
                    System.out.println("Книга " + nameBook + " в данный момент одолжена посетителем!");
                }
            }
            if (!search) { //Если не нашли книгу
                System.out.println("Книги " + nameBook + " нет в библиотеке");
            }
        }
    }

    /** 3.Найти и вернуть книгу по названию
     *
     * @param nameBook название книги
     */
    public void search_BookName(String nameBook) {
        boolean search = false;

        for (Book book : bookList) {
            if (book.getNameBook() == nameBook) { //Если элемент из списка совпадает с введенным именем
                System.out.println("Нашлась книга " + nameBook + " автора " + book.getNameAuthor());
                search = true;
            }
        }
        if (!search) {
            System.out.println("Книги с названием " + nameBook + " нет в библиотеке!");
        }
    }
    /** 4.Найти и вернуть список книг по автору
     *
     * @param nameAuthor
     */
    public void search_BookAuthor(String nameAuthor) {
        ArrayList<Book> authorList = new ArrayList<>(); //Создаем новый пустой список
        for (Book book : bookList) {
            if (book.getNameAuthor() == nameAuthor) { //Если элемент из списка совпадает с введенным именем
                authorList.add(book); //Добавляем в пустой список этот объект
            }
        }
        if (!authorList.isEmpty()) { //Если список не пустой то отображаем его
            System.out.println("Найденныйе книги автора " + nameAuthor + " это:");
            for (Book book : authorList) {
                System.out.println(book.getNameBook() + " ");
            }
        }
        else {
            System.out.println("Автор " + nameAuthor + " отсутствует в библиотеке!");
        }
    }

    /** 5.Одолжить книгу посетителю по названию, если выполнены все условия
     *
     * @param visitor посетитель
     * @param nameBook название книги
     */
    public void lend_Book(Visitor visitor, String nameBook) {
        boolean search = false;

        for (Book book : bookList) {
            if (book.getNameBook() == nameBook) { //Если книга есть
                search = true;

                if (!visitor.isLend_Book()) { //Если у посетителя нет книги (false)

                    if (book.getName_Visitor() == null) { //Если книга не одолжена никому

                        book.setName_Visitor(visitor.getName_Visitor()); //Записываем кто взял книгу

                        if (visitor.isFirstTime()) { //Если в первый раз (true)
                            visitor.setId_Visitor(); //Применяем идентификатор
                            visitor.setFirstTime(false);
                        }
                        book.setId_Visitor(visitor.getId_Visitor()); //Присваиваем книге идентификатор посетителя
                        visitor.setLend_Book(true); //Посетитель взял книгу

                        System.out.println("Книга " + nameBook + " одолжена посетителю с индентификатором " + visitor.getId_Visitor());
                    }
                    else {
                        System.out.println("Книга " + nameBook + " уже одолжена!");
                    }
                }
                else {
                    System.out.println("Посетитель уже взял книгу! Сдайте её для получения новой!");
                }
            }
        }
        if (!search) {
            System.out.println("Книга " + nameBook + " отсутствует в библиотеке!");
        }

    }

    /** 6.Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу
     *
     * @param visitor посетитель
     * @param nameBook название книги
     */
    public void send_Book(Visitor visitor, String nameBook) {
        boolean search = false;

        for (Book book : bookList) {
            if (book.getNameBook() == nameBook) { //Если такая книга нашлась
                search = true;

                if (book.getId_Visitor() != null) { //Если книга одолжена

                    if (book.getId_Visitor() == visitor.getId_Visitor()) { //Если идентификатор книги и посетителя свопадают
                        //Очищаем id и имя взявшего посетителя
                        book.setName_Visitor(null);
                        book.setId_Visitor(null);

                        visitor.setLend_Book(false); //У пользователя нет на руках книги
                        System.out.println("Книга " + nameBook + " возвращена в библиотеку!");

                        grade_Book(book); //Оцениваем книгу

                    }
                    else {
                        System.out.println("Мы не можем принять книгу от другого посетителя!");
                    }
                }
                else {
                    System.out.println("Книга " + nameBook + " в данный момент не одолжена!");
                }
            }
        }
        if (!search) {
            System.out.println("Книги " + nameBook + " нет в данной библиотеке!");
        }
    }

    /** 8.Функционал оценивания книг посетителем при возвращении в
     библиотеку
     *
     * @param book книга которую нужно оценить
     */
    private void grade_Book(Book book) {
        System.out.println();
        System.out.println("Оцените, пожалуйста, книгу " + book.getNameBook() + " от 0 до 10 ");

        Scanner scanner = new Scanner(System.in);
        double grade = -1;

        while ((grade < 0) || (grade > 10)) {
            grade = scanner.nextDouble();
        }

        book.gradeList.add(grade); //Добавляем оценку в список

        System.out.println("Спасибо за вашу оценку!");
    }

    /**Расчет среднего арифметического оценки
     *
     * @param book
     * @return
     */
    public double get_GradeBook(Book book) {
        double sum_grade = 0;
        double sred_SumGrade = 0;

        int size_grade = book.gradeList.size(); //Получить размер списка

        for (int i = 0; i < book.gradeList.size(); i++) { //Суммируем оценки
            sum_grade += book.gradeList.get(i);
        }
        sred_SumGrade = sum_grade / size_grade;

        System.out.println("Средняя оценка книги " + book.getNameBook() + " равна: " + sred_SumGrade);
        return sred_SumGrade;
    }


}
