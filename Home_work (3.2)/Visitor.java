package Hwork_3_Ch2;

public class Visitor {
    private String name_Visitor, id_Visitor; //Имя посетителя и индетификатор
    private boolean lend_Book; //Есть ли у на руках книга
    private boolean firstTime; //Если посетитель берет книгу в первый раз

    private static int sum_Visitor = 0;

    //---------------------------- Конструкторы ----------------------------
    public Visitor(String name_Visitor) { //Создаем экземпляр посетителя с null идентификотором
        this.name_Visitor = name_Visitor;
        this.lend_Book = false;
        this.firstTime = true;
    }

    //---------------------------- Геттеры ----------------------------
    public String getName_Visitor() {
        return name_Visitor;
    }

    public String getId_Visitor() {
        return id_Visitor;
    }

    public boolean isLend_Book() {
        return lend_Book;
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public static int getSum_Visitor() {
        return sum_Visitor;
    }

    //---------------------------- Сеттеры ----------------------------
    public void setId_Visitor() {
        this.id_Visitor = String.valueOf(sum_Visitor); //Присваиваем новое значение идентификатора
        sum_Visitor++; //Увеличиваем идентификатор на 1
    }

    public void setLend_Book(boolean lend_Book) {
        this.lend_Book = lend_Book;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }
}
